To run the project, execute

    $ npm install
    $ npm run build
    $ PORT=4000 SOURCE_DIR=tests/data/ STORAGE_DIR=tests/data/results STORAGE_BASE_URL=http://example.com/files/ npm run start

In another tab,

    $ http get 127.0.0.1:4000/scans/00229
    HTTP/1.1 200 OK
    Connection: keep-alive
    Date: Thu, 13 Feb 2020 15:47:40 GMT
    content-length: 98
    content-type: application/json; charset=utf-8

    {
        "serialNumber": "00229",
        "undercarriage": {
            "url": "http://example.com/files/undercarriage_00229.jpg"
        }
    }

