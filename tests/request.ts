import path from "path";
import { HTTPInjectResponse } from "fastify";
import tmp from "tmp-promise";

import bootstrap from "~/bootstrap";

const tmpobj = tmp.dirSync();

afterAll(async () => {
  await tmpobj.removeCallback();
});

const STORAGE_DIR = tmpobj.name;
const SOURCE_DIR = path.join(__dirname, "data");

const appPromise = bootstrap({
  sourceDir: SOURCE_DIR,
  storageDir: STORAGE_DIR,
  storageBaseURL: "http://example.com/files/"
});

export default {
  httpGet: async <T extends {}>(
    url: string
  ): Promise<HTTPInjectResponse & { json?: T }> => {
    const app = await appPromise;
    const response = await app.inject({
      method: "GET",
      url
    });
    try {
      return { ...response, json: JSON.parse(response.payload) };
    } catch {
      return response;
    }
  }
};
