import request from "./request";

test("Get existing scan", async () => {
  const response = await request.httpGet("/scans/00229");
  expect(response.json).toMatchInlineSnapshot(`
    Object {
      "serialNumber": "00229",
      "undercarriage": Object {
        "url": "http://example.com/files/undercarriage_00229.jpg",
      },
    }
  `);
});

test("Get existing scan with multiple parts", async () => {
  const response = await request.httpGet("/scans/00230");
  expect(response.json).toMatchInlineSnapshot(`
Object {
  "serialNumber": "00230",
  "undercarriage": Object {
    "url": "http://example.com/files/undercarriage_00230.jpg",
  },
  "wheel": Object {
    "url": "http://example.com/files/wheel_00230.jpg",
  },
}
`);
});

test("Attempt to get a scan with unexpected serial number results in 404", async () => {
  const response = await request.httpGet("/scans/whatever");
  expect(response.statusCode).toBe(404);
});
