/* eslint-disable @typescript-eslint/no-use-before-define */
import fs from "fs";
import path from "path";
import tmp from "tmp-promise";

import FileProcessor from "./FileProcessor";

const tmpobj = tmp.dirSync();

afterAll(async () => {
  await tmpobj.removeCallback();
});

const tempDir = tmpobj.name;

describe("fileProcessor", () => {
  let fileProcessor: FileProcessor;

  beforeAll(() => {
    createTempFile("1.jpg");
    createTempFile("2.jpg");
    createTempFile("3.png");
  });

  afterEach(() => {
    fileProcessor.stop();
  });

  it("processes all existing files in a directory", async () => {
    const handler = jest.fn();
    fileProcessor = new FileProcessor({ handler, directory: tempDir });
    await fileProcessor.run();
    expect(handler).toHaveBeenCalledTimes(3);
    expect(handler).toHaveBeenCalledWith(path.join(tempDir, "1.jpg"));
    expect(handler).toHaveBeenCalledWith(path.join(tempDir, "2.jpg"));
    expect(handler).toHaveBeenCalledWith(path.join(tempDir, "3.png"));
  });

  it("allows to choose which of existing files to process", async () => {
    const handler = jest.fn();
    fileProcessor = new FileProcessor({
      handler,
      directory: tempDir,
      filter: (filename: string): boolean => filename.endsWith(".jpg")
    });
    await fileProcessor.run();
    expect(handler).toHaveBeenCalledTimes(2);
    expect(handler).toHaveBeenCalledWith(path.join(tempDir, "1.jpg"));
    expect(handler).toHaveBeenCalledWith(path.join(tempDir, "2.jpg"));
  });

  it("an exception during processing of one of existing files does not interfere with other", async () => {
    const handler = jest.fn(
      async (filepath: string): Promise<void> => {
        if (filepath.endsWith("2.jpg")) {
          throw new Error("Cannot load file 2.jpg");
        }
      }
    );
    const consoleSpy = jest.spyOn(console, "error");
    consoleSpy.mockImplementationOnce(() => unescape);
    fileProcessor = new FileProcessor({
      handler,
      directory: tempDir
    });
    await fileProcessor.run();
    expect(handler).toHaveBeenCalledTimes(3);
    expect(handler).toHaveBeenCalledWith(path.join(tempDir, "1.jpg"));
    expect(handler).toHaveBeenCalledWith(path.join(tempDir, "2.jpg"));
    expect(handler).toHaveBeenCalledWith(path.join(tempDir, "3.png"));
    expect(consoleSpy).toHaveBeenCalledWith(
      new Error("Cannot load file 2.jpg")
    );
    consoleSpy.mockRestore();
  });

  it("processes new files in a directory", async next => {
    const handler = jest.fn();
    fileProcessor = new FileProcessor({ handler, directory: tempDir });
    await fileProcessor.run();
    handler.mockImplementationOnce((filepath: string): void => {
      expect(filepath).toEqual(path.join(tempDir, "new.jpg"));
      next();
    });
    createTempFile("new.jpg");
  });

  it("allows to choose which of new files to process", async () => {
    const handler = jest.fn();
    fileProcessor = new FileProcessor({
      handler,
      directory: tempDir,
      filter: (filename: string): boolean => filename.endsWith(".exe")
    });
    await fileProcessor.run();
    await new Promise((resolve, reject) => {
      handler.mockImplementationOnce(() => {
        reject("Should have not been called");
      });
      createTempFile("new.jpg");
      setTimeout(resolve, 200);
    });
  });

  it("an exception during processing of one of new files does not interfere with other", async () => {
    const handler = jest.fn(
      async (filepath: string): Promise<void> => {
        if (filepath.endsWith("new1.jpg")) {
          throw new Error("Cannot load file new1.jpg");
        }
      }
    );
    const consoleSpy = jest.spyOn(console, "error");
    consoleSpy.mockImplementationOnce(() => undefined);
    fileProcessor = new FileProcessor({
      handler,
      directory: tempDir
    });
    await fileProcessor.run();

    createTempFile("new1.jpg");
    await sleep(200);
    expect(consoleSpy).toHaveBeenCalledWith(
      new Error("Cannot load file new1.jpg")
    );

    handler.mockReset();
    createTempFile("new2.jpg");
    await sleep(300);
    expect(handler).toHaveBeenCalledWith(path.join(tempDir, "new2.jpg"));
  });
});

const createTempFile = (name: string): void =>
  fs.writeFileSync(path.join(tempDir, name), "");

const sleep = (ms: number): Promise<void> =>
  new Promise(resolve => setTimeout(resolve, ms));
