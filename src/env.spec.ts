import env from "./env";

let originalEnv;

beforeEach(() => {
  originalEnv = process.env;
  process.env = {};
});

afterEach(() => {
  process.env = originalEnv;
});

test("PORT is a number", () => {
  process.env.PORT = "31337";
  expect(env.PORT).toEqual(31337);
});

test("PORT is required", () => {
  expect(() => {
    env.PORT;
  }).toThrowErrorMatchingInlineSnapshot(
    `"Environment variable PORT is required"`
  );
});

test("SOURCE_DIR", () => {
  process.env.SOURCE_DIR = __dirname;
  expect(env.SOURCE_DIR).toEqual(__dirname);
});

test("SOURCE_DIR should exist", () => {
  process.env.SOURCE_DIR = "/for/sure/this/directory/does/not/exist";
  expect(() => {
    env.SOURCE_DIR;
  }).toThrowErrorMatchingInlineSnapshot(
    `"Environment variable SOURCE_DIR should point to an existing directory"`
  );
});

test("SOURCE_DIR should be a directory", () => {
  process.env.SOURCE_DIR = __filename;
  expect(() => {
    env.SOURCE_DIR;
  }).toThrowErrorMatchingInlineSnapshot(
    `"Environment variable SOURCE_DIR should point to an existing directory"`
  );
});

test("SOURCE_DIR is required", () => {
  expect(() => {
    env.SOURCE_DIR;
  }).toThrowErrorMatchingInlineSnapshot(
    `"Environment variable SOURCE_DIR is required"`
  );
});

test("STORAGE_DIR should exist", () => {
  process.env.STORAGE_DIR = "/for/sure/this/directory/does/not/exist";
  expect(() => {
    env.STORAGE_DIR;
  }).toThrowErrorMatchingInlineSnapshot(
    `"Environment variable STORAGE_DIR should point to an existing directory"`
  );
});

test("STORAGE_DIR should be a directory", () => {
  process.env.STORAGE_DIR = __filename;
  expect(() => {
    env.STORAGE_DIR;
  }).toThrowErrorMatchingInlineSnapshot(
    `"Environment variable STORAGE_DIR should point to an existing directory"`
  );
});

test("STORAGE_DIR is required", () => {
  expect(() => {
    env.STORAGE_DIR;
  }).toThrowErrorMatchingInlineSnapshot(
    `"Environment variable STORAGE_DIR is required"`
  );
});

test("STORAGE_BASE_URL", () => {
  process.env.STORAGE_BASE_URL = "https://example.com";
  expect(env.STORAGE_BASE_URL).toEqual("https://example.com");
});

test("STORAGE_BASE_URL is required", () => {
  expect(() => {
    env.STORAGE_BASE_URL;
  }).toThrowErrorMatchingInlineSnapshot(
    `"Environment variable STORAGE_BASE_URL is required"`
  );
});
