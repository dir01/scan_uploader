import fs, { FSWatcher } from "fs";
import path from "path";

export default class FileProcessor {
  private directory: string;
  private handler: (filepath: string) => Promise<void>;
  private filter?: (filename: string) => boolean;
  private watcher: FSWatcher;

  constructor(data: {
    directory: string;
    handler: (filepath: string) => Promise<void>;
    filter?: (filename: string) => boolean;
  }) {
    Object.assign(this, data);
  }

  async run(): Promise<void> {
    this.watcher = fs.watch(
      this.directory,
      {},
      (event: string, filename: string) => {
        if (event == "rename") {
          this.processFilename(filename);
        }
      }
    );
    const filenames = fs.readdirSync(this.directory);
    await Promise.all(filenames.map(f => this.processFilename(f)));
  }

  public stop(): void {
    this.watcher.close();
  }

  private async processFilename(filename: string): Promise<void> {
    if (this.filter && !this.filter(filename)) {
      return;
    }
    const filepath = path.join(this.directory, filename);

    if (!fs.lstatSync(filepath).isFile()) {
      return;
    }

    try {
      await this.handler(filepath);
    } catch (e) {
      console.error(e);
    }
  }
}
