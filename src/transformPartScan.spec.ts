/* eslint-disable @typescript-eslint/no-explicit-any */
import { scale } from "./transformations";
import transformPartScan from "./transformPartScan";
import { PartScan } from "./valueObjects";

jest.mock("./transformations", () => ({ scale: jest.fn() }));

test("wheel is scaled down", async () => {
  const wheel = new PartScan({ type: "wheel", serialNumber: "", filepath: "" });
  await transformPartScan(wheel, "/storage");
  expect(scale).toHaveBeenCalledWith({
    partScan: wheel,
    storageDir: "/storage",
    scaleCoefficient: 0.5
  });
});

test("undercarriage is scaled up", async () => {
  const undercarriage = new PartScan({
    type: "undercarriage",
    serialNumber: "",
    filepath: ""
  });
  await transformPartScan(undercarriage, "/storage");
  expect(scale).toHaveBeenCalledWith({
    partScan: undercarriage,
    storageDir: "/storage",
    scaleCoefficient: 2
  });
});

test("attempt to transform unknown part throws an error", async () => {
  expect(
    transformPartScan(({ type: "foo" } as any) as PartScan, "/storage")
  ).rejects.toThrowErrorMatchingInlineSnapshot(`"Unexpected part type foo"`);
});
