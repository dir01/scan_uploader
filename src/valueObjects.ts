export type ScanType = "wheel" | "undercarriage";

export class PartScan {
  public serialNumber: string;
  public type: ScanType;
  public filepath: string;

  constructor(data: {
    serialNumber: string;
    type: ScanType;
    filepath: string;
  }) {
    Object.assign(this, data);
  }
}
