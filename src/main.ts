import bootstrap from "./bootstrap";
import env from "./env";

bootstrap({
  sourceDir: env.SOURCE_DIR,
  storageDir: env.STORAGE_DIR,
  storageBaseURL: env.STORAGE_BASE_URL
}).then(app => {
  app
    .listen(env.PORT, "0.0.0.0")
    .then(address => {
      console.log(`server listening on ${address}`);
    })
    .catch(err => {
      console.log("Error starting server:", err);
      process.exit(1);
    });
});
