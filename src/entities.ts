import { ScanType } from "./valueObjects";

export type FileInfo = {
  filepath: string;
};

export class VehicleScan {
  serialNumber: string;
  undercarriage?: FileInfo;
  wheel?: FileInfo;

  constructor(data: {
    serialNumber: string;
    undercarriage?: FileInfo;
    wheels?: FileInfo;
  }) {
    Object.assign(this, data);
  }

  public isPartScanExists(type: ScanType): boolean {
    return Boolean(this[type]);
  }
}
