import path from "path";
import sharp from "sharp";
import tmp from "tmp-promise";

import { PartScan } from "./valueObjects";
import { scale } from "./transformations";

const tmpobj = tmp.dirSync();

afterAll(async () => {
  await tmpobj.removeCallback();
});

const RESULT_DIR = tmpobj.name;
const TEST_DATA_DIR = path.join(__dirname, "../tests/data");

describe("scale", () => {
  let original: PartScan;
  let transformed: PartScan;

  beforeAll(async () => {
    original = new PartScan({
      type: "wheel",
      serialNumber: "00230",
      filepath: path.join(TEST_DATA_DIR, "wheel_00230.jpg")
    });
    transformed = await scale({
      partScan: original,
      storageDir: RESULT_DIR,
      scaleCoefficient: 0.5
    });
  });

  it("is moved to a storageDir", async () => {
    expect(transformed.filepath).toEqual(
      path.join(RESULT_DIR, "wheel_00230.jpg")
    );
  });

  it("is scaled down 50%", async () => {
    const { width: originalWidth, height: originalHeight } = await sharp(
      original.filepath
    ).metadata();
    const { width: transformedWidth, height: transformedHeight } = await sharp(
      transformed.filepath
    ).metadata();

    expect([originalWidth, originalHeight]).toEqual([3360, 2240]);
    expect([transformedWidth, transformedHeight]).toEqual([1680, 1120]);
  });
});
