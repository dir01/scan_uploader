import path from "path";
import tmp from "tmp-promise";

import VehicleScansService from "./VehicleScansService";
import MemoryVehicleScansRepository from "./MemoryVehicleScansRepository";
import { PartScan } from "./valueObjects";
import VehicleScansRepository from "./MemoryVehicleScansRepository";

const tmpobj = tmp.dirSync();

afterAll(async () => {
  await tmpobj.removeCallback();
});

const RESULT_DIR = tmpobj.name;
const TEST_DATA_DIR = path.join(__dirname, "../tests/data");

describe("ScansService.importPartScan", () => {
  let service: VehicleScansService;
  let repository: VehicleScansRepository;

  beforeAll(async () => {
    repository = new MemoryVehicleScansRepository();
    service = new VehicleScansService(repository, RESULT_DIR);
  });

  it("transforms and imports a part scan", async () => {
    const partScan = new PartScan({
      serialNumber: "00231",
      type: "wheel",
      filepath: path.join(TEST_DATA_DIR, "wheel_00231.jpg")
    });

    await service.importPartScan(partScan);

    const scan = await repository.getBySerialNumber("00231");
    expect(scan).toMatchObject({
      serialNumber: "00231",
      wheel: {
        filepath: path.join(RESULT_DIR, "wheel_00231.jpg")
      }
    });
  });
});
