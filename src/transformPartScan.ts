import { PartScan } from "./valueObjects";
import { scale } from "./transformations";

export default async function transformPartScan(
  partScan: PartScan,
  storageDir: string
): Promise<PartScan> {
  /* If we had a lot of part types and a few possible transformations, we could have 
    made a configuration to map specific part types on specific transformations, 
    but since for now requirements are simplistic, a simlistic solution will suffice */

  if (partScan.type === "undercarriage") {
    return scale({ partScan, storageDir, scaleCoefficient: 2 });
  }

  if (partScan.type === "wheel") {
    return scale({ partScan, storageDir, scaleCoefficient: 0.5 });
  }

  throw new Error(`Unexpected part type ${partScan.type}`);
}
