import { VehicleScan } from "./entities";
import { VehicleScansRepository } from "./VehicleScansService";
import { PartScan } from "./valueObjects";

export default class MemoryVehicleScansRepository
  implements VehicleScansRepository {
  private scans: { [serialNumber: string]: VehicleScan };
  constructor() {
    this.scans = {};
  }

  async getBySerialNumber(serialNumber: string): Promise<VehicleScan> {
    return this.scans[serialNumber];
  }

  async savePartScan(serialNumber: string, partScan: PartScan): Promise<void> {
    if (!this.scans[serialNumber]) {
      this.scans[serialNumber] = new VehicleScan({ serialNumber });
    }
    this.scans[serialNumber][partScan.type] = { filepath: partScan.filepath };
    /* In mongodb this could have been implemented kinda like this:
    await this.db.scans.updateOne(
      {serialNumber},
      {
        serialNumber,
        $set: { [partScan.type]: partScan },
        { upsert: true }
      }
    )
    */
  }
}
