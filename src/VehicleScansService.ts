import { VehicleScan } from "./entities";
import { PartScan } from "./valueObjects";
import transformPartScan from "./transformPartScan";

export interface VehicleScansRepository {
  getBySerialNumber(serialNumber: string): Promise<VehicleScan>;
  savePartScan(serialNumber: string, partScan: PartScan): Promise<void>;
}

export default class VehicleScansService {
  constructor(
    private repository: VehicleScansRepository,
    private scansStorageDir: string
  ) {}

  async importPartScan(partScan: PartScan): Promise<void> {
    /* Here we operate under the assumption that
    1) Part scan might already be imported, that's why we check first
    2) Another part of the same vehicle could be being processed at the same moment,
      that's why we atomically update a part of a scan, not the whole scan.
      We could update the whole as well, but only if our database supported transactions.
    */
    const scan = await this.repository.getBySerialNumber(partScan.serialNumber);
    if (scan && scan.isPartScanExists(partScan.type)) {
      return;
    }
    const transformed = await transformPartScan(partScan, this.scansStorageDir);
    await this.repository.savePartScan(partScan.serialNumber, transformed);
  }

  async getScan(serialNumber: string): Promise<VehicleScan | undefined> {
    return this.repository.getBySerialNumber(serialNumber);
  }
}
