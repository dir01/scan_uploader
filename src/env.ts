import fs from "fs";

class Env {
  get SOURCE_DIR(): string {
    return this.getDir("SOURCE_DIR");
  }

  get STORAGE_DIR(): string {
    return this.getDir("STORAGE_DIR");
  }

  get STORAGE_BASE_URL(): string {
    return this.getEnv("STORAGE_BASE_URL");
  }

  get PORT(): number {
    return parseInt(this.getEnv("PORT"));
  }

  private getDir(name: string): string {
    const dir = this.getEnv(name);
    if (!fs.existsSync(dir) || !fs.lstatSync(dir).isDirectory()) {
      throw new Error(
        `Environment variable ${name} should point to an existing directory`
      );
    }
    return dir;
  }

  private getEnv(name: string): string {
    const val = process.env[name];
    if (!val) {
      throw new Error(`Environment variable ${name} is required`);
    }
    return val;
  }
}

export default new Env();
