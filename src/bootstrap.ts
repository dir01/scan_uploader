import fastify from "fastify";
import path from "path";
import { NotFound } from "http-errors";

import MemoryVehicleScansRepository from "./MemoryVehicleScansRepository";
import VehicleScansService from "./VehicleScansService";
import FileProcessor from "./FileProcessor";
import { PartScan, ScanType } from "./valueObjects";
import { VehicleScan } from "./entities";

export type BootstrapOptions = {
  sourceDir: string;
  storageDir: string;
  storageBaseURL: string;
};

export default async function bootstrap(
  options: BootstrapOptions
): Promise<fastify.FastifyInstance> {
  const repositroy = new MemoryVehicleScansRepository();
  const service = new VehicleScansService(repositroy, options.storageDir);

  const fileProcessorPromise = new FileProcessor({
    directory: options.sourceDir,
    handler: async (filepath: string): Promise<void> => {
      // In production application, more validation would be made around analyzing filename
      const [type, serialNumber] = path.parse(filepath).name.split("_");
      const partScan = new PartScan({
        type: type as ScanType,
        serialNumber,
        filepath
      });
      await service.importPartScan(partScan);
    }
  }).run();

  const app = fastify({});

  app.get("/scans/:serialNumber", async (request, reply) => {
    const serialNumber = request.params.serialNumber;
    const scan = await service.getScan(serialNumber);
    if (!scan) {
      throw NotFound();
    }
    const payload = serializeScan(scan, options.storageBaseURL);
    reply.send(payload);
  });

  await fileProcessorPromise;
  return app;
}

function serializeScan(scan: VehicleScan, baseUrl: string) {
  // In a production application, as number and complexity of responses grow,
  // there would be separate classes responsible for transforming domain entities into responses.
  // For current requirements, this will do.
  // Luckyly, this will be easy to change, since the endpoint is well-tested
  const asUrl = (filepath: string): string => baseUrl + path.basename(filepath);
  return {
    serialNumber: scan.serialNumber,
    wheel: scan.wheel ? { url: asUrl(scan.wheel.filepath) } : undefined,
    undercarriage: scan.undercarriage
      ? { url: asUrl(scan.undercarriage.filepath) }
      : undefined
  };
}
