import sharp from "sharp";
import path from "path";

import { PartScan } from "./valueObjects";

export async function scale({
  partScan,
  storageDir,
  scaleCoefficient
}: {
  partScan: PartScan;
  storageDir: string;
  scaleCoefficient: number;
}): Promise<PartScan> {
  const image = sharp(partScan.filepath);
  const newFilepath = path.join(storageDir, path.basename(partScan.filepath));
  const { width, height } = await image.metadata();
  await image
    .resize({
      width: width * scaleCoefficient,
      height: height * scaleCoefficient
    })
    .toFile(newFilepath);
  return new PartScan({ ...partScan, filepath: newFilepath });
}
